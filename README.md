# Cyber Security News Resources

This is a list of security news sites (in no particular order)

https://www.hackread.com/

https://www.wired.com/tag/cybersecurity/

https://threatpost.com/

https://www.darkreading.com/

https://www.cshub.com/news

https://null-byte.wonderhowto.com/how-to/hack-like-a-pro/

https://cyware.com/cyber-security-news-articles

https://www.scmagazine.com/home/security-news/

https://www.securitymagazine.com/topics/2788-cyber

https://www.securityweek.com/cybercrime

https://thehackernews.com/

https://www.nytimes.com/topic/subject/computer-security-cybersecurity

https://www.technewsworld.com/perl/section/cyber-security/

https://thecyberwire.com/

https://www.theguardian.com/technology/hacking



